tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer iterator = 0;
}
prog    : (e+=expr | d+=decl)* -> program(name={$e}, deklaracje={$d});


decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> addNumbers(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> subtractNumbers(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> multiplyNumbers(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> divideNumbers(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) -> assignVar(p1={$i1.text}, p2={$e2.st})
        | INT                      -> int(i={$INT.text})
        | ID                       -> id(x={$ID.text})
        | ^(IF e1=expr e2=expr e3=expr?) {iterator++;} -> if(cond={$e1.st}, match={$e2.st}, mismatch={$e3.st}, i={iterator.toString()})
    ;
    